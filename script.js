// NAVBAR


document.addEventListener('DOMContentLoaded', function() {
  // Hamburger Toggle
  const hamburger = document.querySelector('.humbarger');
  const menuList = document.querySelector('.menu-list');
  const menuLinks = document.querySelectorAll('.menu-list li a');

  hamburger.addEventListener('click', function(event) {
    menuList.style.display = (menuList.style.display === 'none' || menuList.style.display === '') ? 'block' : 'none';
    event.preventDefault();
  });

  menuLinks.forEach(function(link) {
    link.addEventListener('click', function(event) {
      if (window.innerWidth < 970) {
        menuList.style.display = 'none';
      }
      // Ajoutez ici la logique de redirection vers la page spécifiée dans l'attribut href du lien
      const targetPage = link.getAttribute('href');
      if (targetPage) {
        window.location.href = targetPage;
      }
      event.preventDefault();
    });
  });
});

var lastScrollTop = 0;


    window.addEventListener("scroll", function () {
        var st = window.pageYOffset || document.documentElement.scrollTop;

        
        if (st < lastScrollTop || st > lastScrollTop) {
            // L'utilisateur fait défiler vers le haut
            closeDropdownMenu();
        }
        
        lastScrollTop = st >= 0 ? 0 : st;

    });

    function closeDropdownMenu() {
         if (window.innerWidth <= 970) {
        var menuList = document.querySelector('header .navigation .menu-list');
        menuList.style.display = 'none';
    }
    }

//INDEX PARRALAX EFFECT

// Sélectionnez tous les éléments avec la classe CSS "parallax"
var parallaxElements = document.querySelectorAll('.parallax');

// Calculez la quantité d'éléments parallax
var parallaxQuantity = parallaxElements.length;

// Fonction pour gérer le défilement
function handleScroll() {
  window.requestAnimationFrame(function () {
    // Obtenez la position de défilement verticale
    var scrolled = window.scrollY;

    // Parcourez tous les éléments parallax
    for (var i = 0; i < parallaxQuantity; i++) {
      var currentElement = parallaxElements[i];
      
      // Appliquez la transformation CSS pour l'effet parallax
      currentElement.style.transform = 'translate3d(0,' + (scrolled * -0.3) + 'px, 0)';


    }
  });
}


window.addEventListener('scroll', handleScroll);

handleScroll();

// Sélectionnez tous les éléments avec la classe CSS "parallax"
var parallaxxElements = document.querySelectorAll('.parallaxx');

// Calculez la quantité d'éléments parallax
var parallaxxQuantity = parallaxxElements.length;

// Fonction pour gérer le défilement
function handleScrolll() {
  window.requestAnimationFrame(function () {
    // Obtenez la position de défilement verticale
    var scrolled = window.scrollY;

    // Parcourez tous les éléments parallax
    for (var i = 0; i < parallaxxQuantity; i++) {
      var currentElement = parallaxxElements[i];
      
      // Appliquez la transformation CSS pour l'effet parallax
      currentElement.style.transform = 'translate3d(0,' + (scrolled * -0.3) + 'px, 0)';


    }
  });
}


window.addEventListener('scroll', handleScrolll);

handleScroll();
// CITATIONS
const citations = [
            "Le cerveau n'est pas fait pour donner des résultats, il suit des directions - Richard Bandler",
            "Sois le changement que tu veux voir  - Gandhi",
            "Certaines personnes pensent qu’elles vont se sentir bien si certaines choses arrivent… L’astuce est que vous devez vous sentir bien sans raison particulière. - John Grinder",
            "Quand une personne est alignée avec sa légende personnelle, l’univers tout entier s’efforce de lui faire obtenir ce qu’elle cherche - Paolo Coelho",
            "Les jugements posés sur autrui sont des expressions détournées de nos besoins inassouvis - Marshall Rosenberg",
            "Sur terre il y a 2 choses, celles qu’on sait faire, et celles qu’on ne sait pas encore faire. C’est tout. Je ne vous dis pas que c’est facile, je vous dis que c’est possible - Michel Rolion",
            "Doutez de tout, surtout de ce que je vous dit. - Bouddha",
            "Nous ne sommes pas responsables de ce que nous avons vécu mais nous sommes responsables de ce que nous construisons à partir de ce que nous avons vécu - Jean Paul Sartre",
            "Ce qui trouble les hommes, ce ne sont pas les choses, ce sont les jugements qu'ils portent sur les choses - Epictète",
            "Si vous voulez que la vie vous sourie, apportez-lui d'abord votre bonne humeur - Spinoza",
            "L’herbe n’est pas plus verte ailleurs, elle est verte là où vous arrosez - Laurent Bertin",
            "L’absolu n’existe pas : la réalité est toujours le produit d’une interprétation subjective. - Anthony Robbins",
            "La source de tout amour est le même, l’amour est le même, mais le moyen d’expression est différent - Helen Schuman",
            "La compréhension intellectuelle ne saurait accompagner à elle seule la transformation du cœur - Thomas d’Assembourg",
            "Qui ne se plante jamais n’a aucune chance de pousser - Italus le pin",

        ];

        const container = document.getElementById("citations-container");

        let currentIndex = 0;

function getRandomIndex() {
  return Math.floor(Math.random() * citations.length);
}

function displayQuote(index) {
  container.innerHTML = `<div class="citation">${citations[index]}</div>`;
  
  const nextIndex = (index + getRandomIndex()) % citations.length;
  setTimeout(() => {
    displayQuote(nextIndex);
  }, 10000);
}

// Appel initial avec un index aléatoire
displayQuote(getRandomIndex());


// CAROUSEL SEANCES
$(function() {
    'use strict';

    var li = $('.container').find('li'),
        dis = $('.display'),
        findD = dis.find('div');

    var options = [];
    li.each(function() {
        options.push($(this).html());
    });

    li.on('click', function() {
        var num = $(this).index();

        li.parent().fadeOut('slow', function() {
            dis.fadeIn('slow');
            var allHideText = $('.hideText');
            allHideText.removeClass('hideText');

            // Sélectionnez également les éléments avec la classe 'imge' et masquez-les
            var allImge = $('.imge');
            allImge.hide();
        });

        var x = $(this).html();
        dis.find('div').html(x);

        function navigate() {
            findD.html(li.eq(num).html());
        }

        dis.find('.navigation').on('click', function() {
            if ($(this).hasClass('prev')) {
                --num;
                if (num < 0) {
                    num = options.length - 1;
                }
                navigate();
            } else {
                ++num;
                if (num > options.length - 1) {
                    num = 0;
                }
                navigate();
            }
        });

        dis.find('.close').on('click', function() {
            var allHideText = $('.hideText');
            allHideText.addClass('hideText');

            dis.fadeOut('slow', function() {
                li.parent().fadeIn('slow');
                 var allImge = $('.imge');
            allImge.show();
            });
        });
    });
});



$(function() {
    'use strict';

    var li = $('.container').find('li'),
        dis = $('.display'),
        findD = dis.find('div');

    var options = [];
    li.each(function() {
        options.push($(this).html());
    });

    li.on('click', function() {
        var num = $(this).index();

        li.parent().fadeOut('slow', function() {
            dis.fadeIn('slow');
            var allHideText = $('.hideText');
            allHideText.removeClass('hideText');

            // Sélectionnez également les éléments avec la classe 'imge' et masquez-les
            var allImge = $('.imge');
            allImge.hide();
        });

        var x = $(this).html();
        dis.find('div').html(x);

        function navigate() {
            findD.html(li.eq(num).html());
        }

        dis.find('.navigation').on('click', function() {
            if ($(this).hasClass('prev')) {
                --num;
                if (num < 0) {
                    num = options.length - 1;
                }
                navigate();
            } else {
                ++num;
                if (num > options.length - 1) {
                    num = 0;
                }
                navigate();
            }
        });

        dis.find('.close').on('click', function() {
            var allHideText = $('.hideText');
            allHideText.addClass('hideText');

            dis.fadeOut('slow', function() {
                li.parent().fadeIn('slow');
                 var allImge = $('.imge');
            allImge.show();
            });
        });
    });
});

// POUR S'AMUSER EN GROUPE 

    'use strict';
    const scrollingClass = 'js-event';
    const scrollingActiveClass = scrollingClass + '--active';
    const scrollingInactiveClass = scrollingClass + '--inactive';
    const scrollingTime = 1350;
    let scrollingIsActive = false;
    let currentPage = 1;
    const countOfPages = $(`.${scrollingClass}-page`).length;

    function switchPages() {
        const prefixPage = `.${scrollingClass}-page-`;
        const getPageDomEl = (page = currentPage) => $(`${prefixPage}${page}`);

        scrollingIsActive = true;

        getPageDomEl().removeClass(scrollingInactiveClass).addClass(scrollingActiveClass);
        getPageDomEl(currentPage - 1).addClass(scrollingActiveClass);
        getPageDomEl(currentPage + 1).removeClass(scrollingActiveClass);

        setTimeout(() => {
            scrollingIsActive = false;
        }, scrollingTime);
    }

    function scrollingUp() {
        if (currentPage > 1 ) {
            currentPage--;
            switchPages();
        }
    }

    function scrollingDown() {
        if (currentPage < countOfPages) {
            currentPage++;
            switchPages();
        }
    }

 $('.article-button').on('click', function () {
    const targetPage = parseInt($(this).data('page'));
    console.log(targetPage);
    
    if (targetPage > currentPage) {
        while (currentPage < targetPage) {
            scrollingDown();
        }
    } else if (targetPage < currentPage) {
        while (currentPage > targetPage) {
            scrollingUp();
        }
    }
    // Note: Si targetPage est égal à currentPage, aucune action n'est nécessaire.
});

    function handleScrollEvent(e) {
        const sliderPages = $('.slider-pages');

        if (!scrollingIsActive && sliderPages.is(':hover')) {
            e.preventDefault();
            const delta = e.originalEvent.deltaY || -e.originalEvent.detail;

            if (delta > 0) {
                scrollingDown();
            } else if (delta < 0) {
                scrollingUp();
            }
        }
    }

    function handleKeyEvent(e) {

        if (!scrollingIsActive) {
          e.preventDefault();
            const keyCode = e.which || e.keyCode;
            if (keyCode === 38) {
                scrollingUp();
            } else if (keyCode === 40) {
                scrollingDown();
            }
        }
    }

    $('.slider-pages').hover(
        function () {
            $('body').addClass('no-scroll');
        }
    );

    $(window).on('wheel', function (e) {
        if ($('body').hasClass('no-scroll')) {
            // e.preventDefault();
        }
    });

    // $('.slider-pages').on('mousewheel DOMMouseScroll', handleScrollEvent);
    // $(document).on('keydown', handleKeyEvent);


    // MODAL METAPHORE

    var buttons = document.querySelectorAll(".toggle-button");
var modal = document.querySelector("#modal");

[].forEach.call(buttons, function (button) {
  button.addEventListener("click", function () {
    modal.classList.toggle("off");
  });
});
 document.addEventListener("click", function (event) {
            if (!modal.contains(event.target) && !event.target.classList.contains("toggle-button")) {
                modal.classList.add("off");
            }
        });