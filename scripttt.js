var buttons = document.querySelectorAll(".toggle-button");
var modal = document.querySelector("#modal");

[].forEach.call(buttons, function (button) {
  button.addEventListener("click", function () {
    modal.classList.toggle("off");
  });
});
 document.addEventListener("click", function (event) {
            if (!modal.contains(event.target) && !event.target.classList.contains("toggle-button")) {
                modal.classList.add("off");
            }
        });